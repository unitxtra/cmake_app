#include <iostream>
#include "SomeClass.h"
#include "SomeOtherClass.h"

SomeClass mySomeClass;
SomeOtherClass mySomeOtherClass;

int main() {
    std::cout << "Add stuff" << std::endl;

    int awsome = mySomeClass.addTwoNumbers(5,5); //10
    int alsoAwsome = mySomeOtherClass.addThreeNumbers(5,5,5); //15
    int anotherAwsomeNumber = mySomeClass.subtractFive(10);

    std::cout << "Result 1 ->" << awsome << std::endl;
    std::cout << "Result 2 ->" << alsoAwsome << std::endl;
    std::cout << "Result 3 ->" << anotherAwsomeNumber << std::endl;

    return 0;
}
